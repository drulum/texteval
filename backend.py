import sqlite3


class Database:

    def __init__(self, db_file):
        """This will create the database and tables if they do not already exist in the location specified by db_file"""

        sql_create_analysed_text_table = """CREATE TABLE IF NOT EXISTS analysed_text (
                                                    id integer PRIMARY KEY,
                                                    author text NOT NULL,
                                                    text_title text NOT NULL, 
                                                    chars blob,
                                                    words blob
                                            )"""

        self.db = db_file
        self.conn = sqlite3.connect(self.db)
        self.cur = self.conn.cursor()
        self.cur.execute(sql_create_analysed_text_table)
        self.conn.commit()

    def get_start_id(self):
        try:
            sql = """SELECT MAX(id) FROM analysed_text"""
            self.cur.execute(sql)
            start_id = int(self.cur.fetchone()[0]) + 1
            return start_id
        except TypeError:
            return 1

    def insert(self, data):
        sql = """INSERT INTO analysed_text (id, author, text_title, chars, words) 
                 VALUES (?, ?, ?, ?, ?)"""
        self.cur.execute(sql, data)
        self.conn.commit()

    def select(self, item, item_id):
        sql = "SELECT {} FROM analysed_text WHERE id = {}".format(item, item_id)
        self.cur.execute(sql)
        chars = self.cur.fetchone()[0]
        return chars

    def __del__(self):
        self.conn.close()
