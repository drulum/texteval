import argparse
import pandas as pd
import pickle

from backend import Database

# TODO: this file is going to be solely to process and store dicts in the db
# TODO: create another script to list and retrieve db entries for subsequent stats analysis and output
# TODO: decide how to handle case insensitivity (default? double store in db? process after store?)


def text_from_file(file):
    with open(file) as open_file:
        text = open_file.read()
    return text


def generate_exclusions(exclusions):
    # TODO: default is no exclusion list and using -e argument will load from a default file
    #  specify a different file to load from?
    if exclusions:
        # Default list removes common characters and command codes
        exclusion_list = [' ', '.', ',', '\n']
    else:
        # No exclusions, empty list
        exclusion_list = []
    return exclusion_list


def text_stats(text, exclusions=False, part='c'):
    exclusion_list = generate_exclusions(exclusions)
    if part == 'c':
        part_split = list(text)
    elif part == 'w':
        part_split = text.split()
    else:
        raise ValueError("The variable 'part' must be either 'c' or 'w'.")
    count_dict = {}
    for portion in part_split:
        if portion not in exclusion_list:
            count_dict[portion] = count_dict.get(portion, 0) + 1

    # pickle the dict for storage in a blob
    pickle_dict = pickle.dumps(count_dict)

    return pickle_dict


def texteval(args):
    database = Database(r"textualanalysis.sqlite")

    text = text_from_file(args.file).lower()

    if args.keep:
        print(f'Original text start:\n{text[:400]}...\n\nOriginal text end:\n...{text[-400:]}')

    chars_pickle = text_stats(text)
    words_pickle = text_stats(text, part='w')

    db_tuple = (database.get_start_id(),
                'No author yet',
                'No title yet',
                chars_pickle,
                words_pickle
                )

    database.insert(db_tuple)

    # chars = database.select('chars', database.get_start_id() - 1)
    # chars_dict = pickle.loads(chars)
    # print(pd.Series(chars_dict).to_frame().sort_values(by=0, ascending=False))


def parse_init():
    # TODO: add -o argument to specify output *directory* to contain files for char/words/graphs etc.
    parser = argparse.ArgumentParser(description='Carry out some basic analysis of a plain text file.')

    parser.add_argument('file', help='Specify a single plain text file to evaluate.')
    parser.add_argument('-a', '--all', action='store_true',
                        help='Run all stats. This is the default if no stats are specified.')
    parser.add_argument('-k', '--keep', action='store_true', help='Keep the original text with the stats.')
    parser.add_argument('-c', '--characters', action='store_true', help='Count how many times each character is used.')
    parser.add_argument('-w', '--words', action='store_true', help='Count how many times each word is used.')
    parser.add_argument('-e', '--exclusions', action='store_true',
                        help='Use default exclusion list or specify your own.')
    parser.add_argument('-i', '--insensitive', action='store_true', help='Carry out case insensitive analysis.')

    args = parser.parse_args()

    texteval(args)


if __name__ == '__main__':
    parse_init()
