texteval is a simple script to generate a variety of statistics from a provided text file and then do some form of visualisation on it.

For now, setup a venv and install the requirements and then use the following for a quick test:

python3 texteval.py test_text_2.txt