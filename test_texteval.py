import pickle
import texteval as te


def test_text_from_file():
    """text_from_file() takes a filename as a string, reads the contents and returns it as a single string"""
    text = te.text_from_file('test_text_1.txt')
    assert isinstance(text, str)
    assert text == 'This is a simple test text file.'


def test_generate_exclusions():
    """generate_exclusions() takes a variable to state if exclusions should be used (currently True/False)
    and the exclusions to use"""
    exclusions_false = te.generate_exclusions(False)
    exclusions_true = te.generate_exclusions(True)
    assert isinstance(exclusions_false, list)
    assert isinstance(exclusions_true, list)
    assert exclusions_false == []
    assert exclusions_true == [' ', '.', ',', '\n']


def test_text_stats():
    """text_stats() takes the text to work on as a string, whether or not to remove exclusions from the stats
    and whether the function should process the text as individual characters or words. It returns a dictionary
    containing a pandas dataframe of the text's stats, the total number of chars/words in the text, a count
    of the number of chars/words included in the dataframe and the number of unique chars/words in the dataframe."""
    text_stats_chars = te.text_stats('This is sample text.', exclusions=False)
    text_stats_chars_exclusions = te.text_stats('This is sample text.', exclusions=True)
    text_stats_words = te.text_stats('This is sample text.', exclusions=False, part='w')
    text_stats_words_exclusions = te.text_stats('This is sample text.', exclusions=True, part='w')

    assert pickle.loads(text_stats_chars) == {
        'T': 1,
        'h': 1,
        'i': 2,
        's': 3,
        ' ': 3,
        'a': 1,
        'm': 1,
        'p': 1,
        'l': 1,
        'e': 2,
        't': 2,
        'x': 1,
        '.': 1
    }

    assert pickle.loads(text_stats_words) == {
        'This': 1,
        'is': 1,
        'sample': 1,
        'text.': 1
    }
